# homebridge-noolite-rgb

`homebridge-noolite-rgb` is a plugin for Homebridge.

Control your `noolite`-based RGB light via Homebridge!

## Installation

If you are new to Homebridge, please first read the Homebridge [documentation](https://www.npmjs.com/package/homebridge).
If you are running on a Raspberry, you will find a tutorial in the [homebridge-punt Wiki](https://github.com/cflurin/homebridge-punt/wiki/Running-Homebridge-on-a-Raspberry-Pi).

Install homebridge:
```sh
sudo npm install -g homebridge
```
Install homebridge-noolite-http-rgb:
```sh
sudo npm install -g git+https://bitbucket.org/AlekseevAV/homebridge-noolite-rgb.git
```

## Configuration

Add the accessory in `config.json` in your home directory inside `.homebridge`.

```js
{
  "accessory": "NooLite-RGB",
  "name": "RGB light",
  "nooliteHub": "192.168.1.34:8080",
  "channel": 0
}
```

## Note

This plugin based on [homebridge-fake-rgb](https://www.npmjs.com/package/homebridge-fake-rgb).
