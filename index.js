var request = require('request');
var colorsys = require( 'colorsys' );
var Service, Characteristic;

module.exports = function( homebridge ) {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  homebridge.registerAccessory( "homebridge-noolite-rgb", "NooLite-RGB", RgbAccessory );
};

function RgbAccessory( log, config ) {
  this.log = log;

  // Config
  this.config = config;
  this.name = config.name;
  this.nooliteHub = config.nooliteHub;
  this.channel = config.channel;


  this.power = 0;
  this.brightness = 100;
  this.saturation = 0;
  this.hue = 0;

  this.log( "Initialized '" + this.name + "'" );
}

RgbAccessory.prototype = {
    setColor: function() {
        var color = colorsys.hsv_to_rgb( {
          h: this.hue,
          s: this.saturation,
          v: this.brightness
        } );

        if ( !this.power ) {
          color.r = 0;
          color.g = 0;
          color.b = 0;
        }

        var url = 'http://' + this.nooliteHub + '/api.htm?ch=' + this.channel +
          '&cmd=6&fmt=3&d0=' + color.r + '&d1=' + color.g + '&d2=' + color.b;

        this._httpRequest(url, '', 'GET', function(error, response, body) {
            if (error) {
                this.log('setRGB() failed: %s', error);
            } else {
                this.log('setRGB() successfully set to r: '+ color.r + ' g: ' + color.g + ' b: ' + color.b);
            }
        }.bind(this));

        this.log( "set color to", color.r, color.g, color.b );
    },
    setPower: function (to_power) {
        var power_cmd = null;
        if (to_power == 1) {
          power_cmd = 2
        } else {
          power_cmd = 0
        }
        var url = 'http://' + this.nooliteHub + '/api.htm?ch=' + this.channel + '&cmd=' + power_cmd;

        this._httpRequest(url, '', 'GET', function(error, response, body) {
            if (error) {
                this.log('setPower() failed: %s', error);
            } else {
                this.log('setPower() successfully set to ' + to_power);
            }
        }.bind(this));
    },
    _httpRequest: function(url, body, method, callback) {
        this.log('Sending: ' + url);
        request({
            url: url,
            body: body,
            method: method
        },
        function(error, response, body) {
            callback(error, response, body);
        });
    }
};


RgbAccessory.prototype.getServices = function() {
  var lightbulbService = new Service.Lightbulb( this.name );
  var bulb = this;

  var informationService = new Service.AccessoryInformation();
  informationService
      .setCharacteristic(Characteristic.Manufacturer, 'NooLite')
      .setCharacteristic(Characteristic.Model, 'SD111-180')
      .setCharacteristic(Characteristic.SerialNumber, '0.1');

  lightbulbService
    .getCharacteristic( Characteristic.On )
    .on( 'get', function( callback ) {
      callback( null, bulb.power );
    } )
    .on( 'set', function( value, callback ) {
      bulb.power = value;
      bulb.log( "power to " + value );
      bulb.setPower(value);
      callback();
    } );

  lightbulbService
    .addCharacteristic( Characteristic.Brightness )
    .on( 'get', function( callback ) {
      callback( null, bulb.brightness );
    } )
    .on( 'set', function( value, callback ) {
      bulb.brightness = value;
      bulb.log( "brightness to " + value );
      bulb.setColor();
      callback();
    } );

  lightbulbService
    .addCharacteristic( Characteristic.Hue )
    .on( 'get', function( callback ) {
      callback( null, bulb.hue );
    } )
    .on( 'set', function( value, callback ) {
      bulb.hue = value;
      bulb.log( "hue to " + value );
      bulb.setColor();
      callback();
    } );

  lightbulbService
    .addCharacteristic( Characteristic.Saturation )
    .on( 'get', function( callback ) {
      callback( null, bulb.saturation );
    } )
    .on( 'set', function( value, callback ) {
      bulb.saturation = value;
      bulb.log( "saturation to " + value );
      // bulb.setColor();
      callback();
    } );

  return [ informationService, lightbulbService ];
};
